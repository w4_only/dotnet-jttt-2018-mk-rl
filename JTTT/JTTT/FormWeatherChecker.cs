﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace JTTT
{
    public partial class FormWeatherChecker : Form
    {
        public FormWeatherChecker()
        {
            InitializeComponent();
        }

        private void buttonCheckCity_Click(object sender, EventArgs e)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q=" + textBoxCity.Text + ",pl&APPID=56c179f8287e3a554f0ac888de853e38");
                    var weather = JsonConvert.DeserializeObject<WeatherInfo>(json);
                    textBoxWeatherInformation.Text = "Dzisiaj w " + weather.name + " jest " + (weather.main.temp-273).ToString() + " st. C." + Environment.NewLine +
                        "Cisnienie atmosferyczne wynosi " + weather.main.pressure + "hPa" + Environment.NewLine +
                        "Wiatr wieje z prędkością " + weather.wind.speed + "km/h";

                    pictureBoxWeatherIcon.ImageLocation = "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
                }
            }
            catch
            {
                MessageBox.Show("Prawdopodobnie nieprawidlowa nazwa miasta!");
            }
        }
    }
}
