﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace JTTT
{
    public class TaskDbContext : DbContext
    {
        public TaskDbContext() : base("database20")
        {
            Database.SetInitializer(new TaskDbInitializer());
        }

        public DbSet<Request> Request { get; set; }
        public DbSet<Action> Action { get; set; }
        public DbSet<Task> Task { get; set; }

    }
}
