﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class RequestImage : Request
    {
        public string Url { get; set; }
        public string Text { get; set; }

        public RequestImage() { }
        public RequestImage(string newUrl, string newText)
        {
            Url = newUrl;
            Text = newText;
        }

        public override string ToString()
        {
            return "     URL=" + Url + "     TEXT=" + Text; 
        }
    }
}
