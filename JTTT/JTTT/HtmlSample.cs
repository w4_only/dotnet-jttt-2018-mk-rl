﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


namespace JTTT
{
    /*************************************************************KLASA DO EMAILA****************************************************************************/
    public class HtmlSample
    {
        private readonly string _url;

        public HtmlSample(string url)
        {
            this._url = url;
        }

        //Metoda zwraca zawartość HTML podanej strony www
        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));
                return html;
            }
        }
        //Nu tu ugolnie zapisuje obrazek z url xd
        public string Save(string uri, string nameFile)
        {
            using (WebClient wC = new WebClient())
            {
                wC.DownloadFile(uri, nameFile);
                return nameFile;
            }
        }


        public string FindImage(string text)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();

            HtmlSample docHtml = new HtmlSample(_url);

            //Pobieranie zawartosci strony
            var pageHtml = docHtml.GetPageHtml();

            //Zaladowanie zawartosci strony do obiektu HtmlAgilityPack
            doc.LoadHtml(pageHtml);

            // Metoda Descendants pozwala wybrać zestaw node'ów o określonej nazwie
            var nodes = doc.DocumentNode.Descendants("img");
            string filename;

            foreach (var node in nodes)
            {

                //Jeżeli znalazł obrazek z nazwą podaną w polu Tekst
                if (node.GetAttributeValue("alt", "").Contains(text))
                {
                    //Pobieramy zroblo do obazka
                    string src = node.GetAttributeValue("src", "");
                    return filename = docHtml.Save(src, text + ".jpg");
                }
            }
            return null;
        }
    }

}
