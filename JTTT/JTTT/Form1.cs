﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace JTTT
{
    public partial class Form1 : Form
    {
        BindingList<Task> myListOfTask = new BindingList<Task>();

        public Form1()
        {
            InitializeComponent();
            listBox1.DataSource = myListOfTask; //podanie zródła danych do listbox1
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tabControlRequest.SelectedTab == tabPage1)
            {
                //Sprawdzenie czy podano wszystkie elementy
                if (this.textBox1.Text == "")
                {
                    MessageBox.Show("Uzupelnij pole URL!");
                }
                else if (this.textBox2.Text == "")
                {
                    MessageBox.Show("Uzupelnij pole Tekst!");
                }
                else if (this.textBox4.Text == "")
                {
                    MessageBox.Show("Uzupelnij pole Task!");
                }
                //Jezeli podano wszystkie wartosc
                else
                {
                    if (tabControlAction.SelectedTab == tabPage3)
                    {
                        if (this.textBox3.Text == "")
                        {
                            MessageBox.Show("Uzupelnij pole E-mail!");
                        }
                        else
                        {
                            //tworzenie nowego żądania
                            RequestImage requestImage = new RequestImage(textBox1.Text.ToString(), textBox2.Text.ToString());
                            ActionSendEmail actionSendEmail = new ActionSendEmail(textBox3.Text.ToString());
                            Task newTask = new Task(textBox4.Text.ToString(), requestImage, actionSendEmail);
                            myListOfTask.Add(newTask);
                        }

                    }
                    else if (tabControlAction.SelectedTab == tabPage4)
                    {
                        RequestImage requestImage = new RequestImage(textBox1.Text.ToString(), textBox2.Text.ToString());
                        ActionShow actionShow = new ActionShow();
                        Task newTask = new Task(textBox4.Text.ToString(), requestImage, actionShow);
                        myListOfTask.Add(newTask);
                    }
                }
            }
            else if (tabControlRequest.SelectedTab == tabPage2)
            {
                //Sprawdzenie czy podano wszystkie elementy
                if (this.textBoxCity.Text == "")
                {
                    MessageBox.Show("Uzupelnij pole Miasto!");
                }
                else
                {
                    if (tabControlAction.SelectedTab == tabPage3)
                    {
                        if (this.textBox3.Text == "")
                        {
                            MessageBox.Show("Uzupelnij pole E-mail!");
                        }
                        else
                        {
                            RequestTemperature requestTemperature = new RequestTemperature(textBoxCity.Text.ToString(), (int)numericUpDownTemp.Value);
                            ActionSendEmail actionSendEmail = new ActionSendEmail(textBox3.Text.ToString());
                            Task newTask = new Task(textBox4.Text.ToString(), requestTemperature, actionSendEmail);
                            myListOfTask.Add(newTask);
                        }
                    }
                    else if (tabControlAction.SelectedTab == tabPage4)
                    {
                        RequestTemperature requestTemperature = new RequestTemperature(textBoxCity.Text.ToString(), (int)numericUpDownTemp.Value);
                        ActionShow actionShow = new ActionShow();
                        Task newTask = new Task(textBox4.Text.ToString(), requestTemperature, actionShow);
                        myListOfTask.Add(newTask);


                    }
                }



            }
        }

        private void wykonaj_buttom_Click(object sender, EventArgs e)
        {

            if (myListOfTask.Count > 0)
            {
                DoAction newAction = new DoAction();
                for (int i = 0; i < myListOfTask.Count; i++)
                    newAction.Do(myListOfTask[i].Request, myListOfTask[i].Action, textBoxEmail.Text, textBoxPassword.Text, textBoxSmtp.Text);
            }

        }



        //Przycisk obsluguje czyszczenie listy zadan listOfParts
        private void czysc_buttom_Click(object sender, EventArgs e)
        {
            myListOfTask.Clear();
        }

        //Przycisk obsluguje serializacje listy zadan listOfParts do pliku Serialize.dat
        private void serializacja_button_Click(object sender, EventArgs e)
        {
            TaskDb dataBase = new TaskDb();

            if (myListOfTask.Count > 0)
                dataBase.AddTaskToDb(myListOfTask);
            else
                MessageBox.Show("Brak zadań na liście!");
        }

        //Przycisk obsluguje deserializcje pliku Serialize.dat do listy listOfParts
        private void deserializacja_button_Click(object sender, EventArgs e)
        {
            TaskDb dataBase = new TaskDb();
            myListOfTask = dataBase.GetTaskFromDb();
            listBox1.DataSource = myListOfTask;
        }

        /// <summary>
        /// Przycisk sprawdza pogode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonChecktWeather_Click(object sender, EventArgs e)
        {
            FormWeatherChecker formWeatherChecker = new FormWeatherChecker();
            formWeatherChecker.ShowDialog();
        }
    }
}
