﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


namespace JTTT
{
    /// <summary>
    /// Klasa odpowiada za serializacje i deserializacje BindingList
    /// </summary>
    public class DataSerialize
    {
        /// <summary>
        /// Metoda serializuje elementy z BindingList podanej w argumencie
        /// </summary>
        /// <param name="listOfParts">Serializowany BildingList</param>
        public void SerializeData(BindingList<Request> listOfParts)
        {
            //Otwieranie pliku Serialize.dat, do ktorego bedziemy wczytywac dane 
            FileStream fs = new FileStream("Serialize.dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, listOfParts);
                MessageBox.Show("Serializacja " + listOfParts.Count + " elementów.");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Bład serializacji.");
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        /// <summary>
        /// Metoda deserializuje elementy do BindingList podanej w argumencie
        /// </summary>
        /// <param name="listOfParts">Deserializowany BildingList</param>
        public void DeserializeData(BindingList<Request> listOfParts)
        {
            //Otwieranie pliku Serialize.dat, z ktorego bedziemy czytac dane 
            FileStream fs = new FileStream("Serialize.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                BindingList<Request> temp; //Pomocnicza BindingList, do ktorej bede czytac dane
                temp = (BindingList<Request>)formatter.Deserialize(fs); //Czytanie danych
                int temp_size = temp.Count; //Sprawdzam ilosc pobranych danych
                Request[] tab = new Request[temp_size]; //Tworze tablice do przechowywania danych
                temp.CopyTo(tab, 0); //Kopiuje zawartosc temp do tablicy, zaczynajkąc od indeksu 0
                //Dodawanie do listOfParts wczytanych z deserializacji zadan
                for (int i = 0; i < temp_size; i++)
                    listOfParts.Add(tab[i]);
                MessageBox.Show("Deserializacja " + temp_size + " elementów.");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Bład deserializacji.");
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

    }
}
