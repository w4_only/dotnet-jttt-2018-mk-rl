﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    public class EmailSender
    {
        private string Login; //zmienna przechowuje login do emaila
        private string Password; //zmienna przechowuje haslo do emaila 
        private string Smtp;
        /// <summary>
        /// </summary>
        /// <param name="myLogin">Login do konta, z ktorego chcesz wyslac email</param>
        /// <param name="myEmail">Haslo do konta, z ktorego chcesz wyslac email/param>
        public EmailSender(string myLogin, string myPassword, string mySmtp)
        {
            Smtp = mySmtp;
            Login = myLogin;
            Password = myPassword;
        }
        
        /// <summary>
        /// Metoda wysyla email 
        /// </summary>
        /// <param name="nameFile"></param>
        /// <param name="adressto"></param>
        private void SendEmail(string nameFile, string adressto)
        {
            string file = nameFile;
            //wyslanie email od nadawcy do odbiorcy

            using (MailMessage mess = new MailMessage(Login, adressto))
            {
                //temat, tresc 
                mess.Subject = "Dobry memuch";
                mess.Body = "smich";
                mess.IsBodyHtml = false;

                //zse45rdx
                //kowalskijanxxxx@gmail.com

                //dodaje zalocznik typu .jpg
                Attachment a = new Attachment(nameFile, System.Net.Mime.MediaTypeNames.Image.Jpeg);
                mess.Attachments.Add(a);

                //klient logowania i dane do konta z ktoregomamy wyslac
                using (SmtpClient post = new SmtpClient())
                {
                    post.Credentials = new NetworkCredential(Login, Password);
                    post.Host = Smtp;
                    post.Send(mess);
                }
            }
        }

        /// <summary>
        /// Metoda przeszukuje node's w poszukiwaniu odpowiedzniego obrazka i wysyla emial 
        /// </summary>
        /// <param name="myRequest"></param>
        public void CreatEmail(ActionSendEmail myAction, RequestImage myRequest)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();

            HtmlSample docHtml = new HtmlSample(myRequest.Url);

            //Pobieranie zawartosci strony
            var pageHtml = docHtml.GetPageHtml();

            //Zaladowanie zawartosci strony do obiektu HtmlAgilityPack
            doc.LoadHtml(pageHtml);

            // Metoda Descendants pozwala wybrać zestaw node'ów o określonej nazwie
            var nodes = doc.DocumentNode.Descendants("img");

            Logger newLog = new Logger("jttt_logger.txt");

            int counter = 0;

            foreach (var node in nodes)
            {
                
                //Jeżeli znalazł obrazek z nazwą podaną w polu Tekst
                if (node.GetAttributeValue("alt", "").Contains(myRequest.Text))
                {
                    //Pobieramy zroblo do obazka
                    string src = node.GetAttributeValue("src", "");
                    string filename;
                    newLog.createNewLog("Wyslano email na adres: " + myAction.Email + ", z obrazkiem o tematyce: " + myRequest.Text + ", ze strony: " + myRequest.Url);
                    {
                        filename = docHtml.Save(src, myRequest.Text + ".jpg");
                        SendEmail(filename, myAction.Email);
                    }
                    MessageBox.Show("Wysłano email !");
                    counter++;
                }

            }
        }


        /// <summary>
        /// Metoda wysyla email 
        /// </summary>
        /// <param name="nameFile"></param>
        /// <param name="adressto"></param>
        private void SendEmail(string adressto, RequestTemperature myRequest)
        { 
            using (MailMessage mess = new MailMessage(Login, adressto))
            {
                //temat, tresc 
                mess.Subject = "Informacja o pogodzie";
                mess.Body = "W " + myRequest.City + " temperatura przekroczyla " + myRequest.Temperature + " stopni Celcjusza";
                mess.IsBodyHtml = false;

                //klient logowania i dane do konta z ktoregomamy wyslac
                using (SmtpClient post = new SmtpClient())
                {
                    post.Credentials = new NetworkCredential(Login, Password);
                    post.Host = Smtp;
                    post.Send(mess);
                }
            }
        }

        /// <summary>
        /// Metoda wysyla email na temat pogody na podany adres email
        /// </summary>
        /// <param name="myAction"></param>
        /// <param name="myRequest"></param>
        public void CreatEmail(ActionSendEmail myAction, RequestTemperature myRequest)
        {
            Logger newLog = new Logger("jttt_logger.txt");
            newLog.createNewLog("Wyslano email na adres: " + myAction.Email + ", na temat pogody w miejscowosci: " + myRequest.City + ", na temat przekroczenia temperatury: " + myRequest.Temperature);

            SendEmail(myAction.Email,myRequest);
            MessageBox.Show("Wysłano email !");

        }

    }
}
