﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Task
    {
        public int TaskId { get; set; }
        public int ActionId { get; set; }
        public int RequestId { get; set; }
        public string TaskName { get; set; }

        public virtual Action Action { get; set; }
        public virtual Request Request { get; set; }

        public Task() { }
        public Task(string newTaskName,Request newRequest,Action newAction)
        {
            TaskName = newTaskName;
            Request = newRequest;
            Action = newAction;
        }

        public void Add(Task newTask)
        {
            TaskId = newTask.TaskId;
            ActionId = newTask.ActionId;
            RequestId = newTask.RequestId;
            TaskName = newTask.TaskName;
            Action = newTask.Action;
            Request = newTask.Request;
        }

        public override string ToString()
        {
            return "TASK=" + TaskName + Request.ToString() + Action.ToString();
        }

    }
}
