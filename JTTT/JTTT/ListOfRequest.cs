﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    [Serializable]
    public class ListOfRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Zmienna przechowuje BindingList zadań.
        /// </summary>
        public virtual BindingList<Request> Requests { get; set; } = new BindingList<Request>();

        /// <summary>
        /// Geter do BindingList
        /// </summary>
        public BindingList<Request> SourceListOfRequest
        {
            get
            {
                return Requests;
            }

        }

        /// <summary>
        ///Metoda czyści liste z zadań 
        /// </summary>
        public void ClearRequest()
        {
            Requests.Clear();
        }

        /// <summary>
        /// Metoda zwraca ilość przechowywanych zadań
        /// </summary>
        /// <returns></returns>
        public int numberOfRequest()
        {
            return Requests.Count;
        }

        /// <summary>
        /// Metoda zwraca opis zawartości listy zadań
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "ID:" + Id + "  NAME:" + Name;
        }

    }
}
