﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class TaskDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<TaskDbContext>
    {

        protected override void Seed(TaskDbContext context)
        {
            RequestImage newRequestImage = new RequestImage("https://jbzdy.pl", "podatki");
            RequestTemperature newRequestTemperature = new RequestTemperature("Wroclaw", 5);
            ActionSendEmail newActionSendEmail = new ActionSendEmail("m.krzyzak.95@gmail.com");
            ActionShow newActionShow = new ActionShow();

            Task newTask;
            
            newTask = new Task() { TaskId = 1, ActionId = 1, RequestId = 1, TaskName = "test", Request = newRequestImage, Action = newActionSendEmail  };
            context.Task.Add(newTask);

            newTask = new Task() { TaskId = 2, ActionId = 2, RequestId = 2, TaskName = "test", Request = newRequestImage, Action = newActionShow };
            context.Task.Add(newTask);

            newTask = new Task() { TaskId = 3, ActionId = 3, RequestId = 3, TaskName = "test", Request = newRequestTemperature, Action = newActionSendEmail };
            context.Task.Add(newTask);

            newTask = new Task() { TaskId = 4, ActionId = 4, RequestId = 4, TaskName = "test", Request = newRequestTemperature, Action = newActionShow };
            context.Task.Add(newTask);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}