﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class FormActionDisplay : Form
    {
        public FormActionDisplay(string task, string picture)
        {
            InitializeComponent();
            textBoxTask.Text = task;
            pictureBoxImage.ImageLocation = picture;
        }
    }
}
