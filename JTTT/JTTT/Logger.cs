﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    /// <summary>
    /// Klasa odpowiada za wykonywanie logow
    /// </summary>
    public class Logger
    {
        private string fileName; //zmienna przechowuje nazwe pliku, do ktorego maja byc wyslane logi
        
        /// <summary>
        /// Konstruktor w argumencie ktorego podaje sie sciezke do jakiej ma byc zapisany log.
        /// </summary>
        /// <param name="myFileName">Sciezka do pliku</param>
        public Logger(string myFileName)
        {
            fileName = myFileName;
        }
        public void createNewLog(string lines)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, true);
            file.WriteLine(lines);
            file.Close();
        }
    }
}
