﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    public class TaskDb
    {
        /// <summary>
        /// Metoda dodaje liste elementow do bazy danych.
        /// </summary>
        /// <param name="listOfRequest">Lista elementow, ktora chcemy oddac</param>
        /// <param name="id">Id listy, które nadajemy liście</param>
        /// <param name="name">Nazwa listy, którą nadajemy liście.</param>
        public void AddTaskToDb(BindingList<Task> newTaskList)
        {
            using (var context = new TaskDbContext())
            {
                for (int i = 0; i < newTaskList.Count; i++)
                {
                    context.Task.Add(newTaskList[i]);
                    context.SaveChanges();
                }
            }
        }


        /// <summary>
        /// Funckja zwraca liste elementow z bazy danych o podanych w argumencie parametrach.
        /// </summary>
        /// <param name="id">Id listy, ktora chcemy odszukac w bazie</param>
        /// <param name="name">Nazwa listy, którą chcemy odszukać w bazie</param>
        /// <returns>ListOfRequest jeżeli znjdzie liste o odpowiednich parametrach. Null jezeli nie znajdzie listy o odpowiednich parametrach.</returns>
        public BindingList<Task> GetTaskFromDb()
        {
            BindingList<Task> newTaskList = new BindingList<Task>();

            using (var context = new TaskDbContext())
            {
                foreach (var t in context.Task)
                {
                    Task newTask = new Task();

                    foreach (var a in context.Action)
                        if (a.ActionId == t.ActionId)
                            newTask.Action = a;

                    foreach (var r in context.Request)
                        if (r.RequestId == t.RequestId)
                            newTask.Request = r;

                    newTask.TaskName = t.TaskName;
                    newTaskList.Add(newTask);
                }

                if (newTaskList.Count > 0)
                    return newTaskList;
                else
                    return null;
            }
        }


    }
}
