﻿namespace JTTT
{
    partial class FormWeatherChecker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCity = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.buttonCheckCity = new System.Windows.Forms.Button();
            this.textBoxWeatherInformation = new System.Windows.Forms.TextBox();
            this.pictureBoxWeatherIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWeatherIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCity.Location = new System.Drawing.Point(10, 8);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(76, 25);
            this.labelCity.TabIndex = 0;
            this.labelCity.Text = "Miasto:";
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(92, 13);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(174, 20);
            this.textBoxCity.TabIndex = 1;
            // 
            // buttonCheckCity
            // 
            this.buttonCheckCity.Location = new System.Drawing.Point(12, 51);
            this.buttonCheckCity.Name = "buttonCheckCity";
            this.buttonCheckCity.Size = new System.Drawing.Size(470, 31);
            this.buttonCheckCity.TabIndex = 2;
            this.buttonCheckCity.Text = "Jaka jest pogoda?";
            this.buttonCheckCity.UseVisualStyleBackColor = true;
            this.buttonCheckCity.Click += new System.EventHandler(this.buttonCheckCity_Click);
            // 
            // textBoxWeatherInformation
            // 
            this.textBoxWeatherInformation.Location = new System.Drawing.Point(12, 100);
            this.textBoxWeatherInformation.Multiline = true;
            this.textBoxWeatherInformation.Name = "textBoxWeatherInformation";
            this.textBoxWeatherInformation.Size = new System.Drawing.Size(339, 134);
            this.textBoxWeatherInformation.TabIndex = 3;
            // 
            // pictureBoxWeatherIcon
            // 
            this.pictureBoxWeatherIcon.Location = new System.Drawing.Point(357, 100);
            this.pictureBoxWeatherIcon.Name = "pictureBoxWeatherIcon";
            this.pictureBoxWeatherIcon.Size = new System.Drawing.Size(125, 134);
            this.pictureBoxWeatherIcon.TabIndex = 4;
            this.pictureBoxWeatherIcon.TabStop = false;
            // 
            // FormWeatherChecker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 251);
            this.Controls.Add(this.pictureBoxWeatherIcon);
            this.Controls.Add(this.textBoxWeatherInformation);
            this.Controls.Add(this.buttonCheckCity);
            this.Controls.Add(this.textBoxCity);
            this.Controls.Add(this.labelCity);
            this.Name = "FormWeatherChecker";
            this.Text = "FormWeatherChecker";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWeatherIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Button buttonCheckCity;
        private System.Windows.Forms.TextBox textBoxWeatherInformation;
        private System.Windows.Forms.PictureBox pictureBoxWeatherIcon;
    }
}