﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class RequestDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<RequestDbContext>
    {

        protected override void Seed(RequestDbContext context)
        {
            ListOfRequest myListOfRequest;

            myListOfRequest = new ListOfRequest() { Id = 1,  Name = "Default list od request 1" };
            //myListOfRequest.Requests.Add(new Request() { Id = 1, ListOfRequestId = 1, task = "podatki", text = "Nowy_mem_o_podatkach", url = "https://demotywatory.pl", email = "m.krzyzak.95@gmail.com" });
            //myListOfRequest.Requests.Add(new Request() { Id = 2, ListOfRequestId = 1, task = "mandatt", text = "Nowy_mem_o_mandatach", url = "https://demotywatory.pl", email = "m.krzyzak.95@gmail.com" });
            context.ListOfRequest.Add(myListOfRequest);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}