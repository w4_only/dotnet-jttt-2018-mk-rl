﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class ActionSendEmail : Action
    {
        public string Email { get; set; }


        public ActionSendEmail() { }
        public ActionSendEmail(string newEmail)
        {
            Email = newEmail;
        }

        public override string ToString()
        {
            return "     Wysylanie emaila,     EMAIL=" + Email;
        }
    }
}
