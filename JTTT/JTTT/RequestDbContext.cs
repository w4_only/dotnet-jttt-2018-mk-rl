﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace JTTT
{
    public class RequestDbContext : DbContext
    {
        public RequestDbContext() : base("JTTTDataBase")
        {
            Database.SetInitializer(new RequestDbInitializer());
        }

        public DbSet<Request> Request { get; set; }

        public DbSet<ListOfRequest> ListOfRequest { get; set; }

    }
}
