﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.IO;

namespace JTTT
{
    public class DoAction
    {
        public void Do(Request myRequest, Action myAction, string email, string pasword, string smtp)
        {
            if (myRequest is RequestImage)
            {
                if (myAction is ActionSendEmail)
                {
                    DoAction newAction = new DoAction();
                    EmailSender myEmailSender = new EmailSender(email, pasword, smtp);
                    myEmailSender.CreatEmail((ActionSendEmail)myAction, (RequestImage)myRequest);
                }
                else if (myAction is ActionShow)
                {
                    RequestImage temp = (RequestImage)myRequest;
                    HtmlSample html = new HtmlSample(temp.Url);
                    string filename = html.FindImage(temp.Text);
                    if (filename == null)
                    {
                        MessageBox.Show("Nie znaleziono obrazka!");
                    }
                    else
                    {
                        FormActionDisplay actionDisplay = new FormActionDisplay(temp.Text, Directory.GetCurrentDirectory() + "\\" + filename);
                        actionDisplay.ShowDialog();
                    }
                }

            }
            else if (myRequest is RequestTemperature)
            {
                RequestTemperature temp = (RequestTemperature)myRequest;
                using (WebClient wc = new WebClient())
                {
                    try
                    {
                        var json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q=" + temp.City + ",pl&APPID=56c179f8287e3a554f0ac888de853e38");
                        var weather = JsonConvert.DeserializeObject<WeatherInfo>(json);

                        if (myAction is ActionSendEmail)
                        {
                            EmailSender myEmailSender = new EmailSender(email, pasword, smtp);
                            if (myAction is ActionSendEmail)
                            {
                                if ((weather.main.temp - 273) > temp.Temperature)
                                {
                                    myEmailSender.CreatEmail((ActionSendEmail)myAction, (RequestTemperature)myRequest);
                                }
                                else
                                {
                                    MessageBox.Show("Temperatura jest zbyt niska");
                                }
                            }
                        }

                        else if (myAction is ActionShow)
                        {
                            if ((weather.main.temp - 273) > temp.Temperature)
                            {
                                string text = "Dzisiaj w " + weather.name + " jest " + (weather.main.temp - 273).ToString() + " st. C." + Environment.NewLine +
                                               "Cisnienie atmosferyczne wynosi " + weather.main.pressure + "hPa" + Environment.NewLine +
                                                  "Wiatr wieje z prędkością " + weather.wind.speed + "km/h";
                                string picture = "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
                                FormActionDisplay actionDisplay = new FormActionDisplay(text, picture);
                                actionDisplay.ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show("Temperatura jest zbyt niska");
                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Prawdopodobnie nieprawidlowa nazwa miasta!");
                    }

                }


            }

        }


    }
}
