﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class RequestTemperature : Request
    {
        public string City { get; set; }
        public int Temperature { get; set; }

        public RequestTemperature() { }
        public RequestTemperature(string newCity, int newTemperature)
        {
            City = newCity;
            Temperature = newTemperature;
        }

        public override string ToString()
        {
            return "    CITY=" + City + "     TEMP.:" + Temperature;
        }

    }
}
