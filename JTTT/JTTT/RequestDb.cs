﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    public class RequestDb
    {
        /// <summary>
        /// Metoda dodaje liste elementow do bazy danych.
        /// </summary>
        /// <param name="listOfRequest">Lista elementow, ktora chcemy oddac</param>
        /// <param name="id">Id listy, które nadajemy liście</param>
        /// <param name="name">Nazwa listy, którą nadajemy liście.</param>
        public void AddRequestToDb(ListOfRequest listOfRequest, string name)
        {
            listOfRequest.Name = name;
            using (var context = new RequestDbContext())
            {
                context.ListOfRequest.Add(listOfRequest);
                context.SaveChanges();
            }

        }

        /// <summary>
        /// Funckja zwraca liste elementow z bazy danych o podanych w argumencie parametrach.
        /// </summary>
        /// <param name="id">Id listy, ktora chcemy odszukac w bazie</param>
        /// <param name="name">Nazwa listy, którą chcemy odszukać w bazie</param>
        /// <returns>ListOfRequest jeżeli znjdzie liste o odpowiednich parametrach. Null jezeli nie znajdzie listy o odpowiednich parametrach.</returns>
        public ListOfRequest GetRequestFromDb(int id, ListOfRequest list)
        {
            using (var context = new RequestDbContext())
            {
                foreach(var l in context.ListOfRequest)
                {
                    if((l.Id == id))
                    {
                        foreach(var r in l.Requests)
                            list.Requests.Add(r);

                        return list;
                    }
                }
                return null;

            }
        }
        public virtual BindingList<Request> Requests { get; set; } = new BindingList<Request>();

        /// <summary>
        /// Metoda zwraca liste  ListOfRequest znajdujących się w bazie danych
        /// </summary>
        /// <returns></returns>
        public List<ListOfRequest> GetListOfRequestFromDb()
        {
            List<ListOfRequest> list = new List<ListOfRequest>();
            using (var context = new RequestDbContext())
            {
                foreach (var l in context.ListOfRequest)
                    list.Add(l);

                return list;
            }
        }

    }
}
